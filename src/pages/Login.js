import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';



export default function Login() {

	// Allows us to consume the user context object and it's properties to be used for validation.
	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);


	function userLogin(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			// We will recieve either token or false response
			console.log(typeof data.access)

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
				
				setEmail("")
				setPassword("")

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to zuitt!"
				})

			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please check your login details and try again."
				})
			}

		})


		// Set email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/
		// localStorage.setItem("email", email);

		// Sets the global user state to have properties obtained from local storage.
		// setUser({email: localStorage.getItem('email')})


	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			// Global user state for validation accross the whole app
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
	

	useEffect(() => {
	 email !== "" && password !== "" ? setIsActive(false) : setIsActive(true)
	},[email, password])

	return (
		(user.id !== null) ?
			<Navigate to="/courses" />
			:
			<Form onSubmit={e => userLogin(e)}>
			      <Form.Group className="mb-3" controlId="email">
			      	<h1>Login</h1>
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        type="email" 
			        placeholder="Enter email" 
			        value={email}
			        onChange={e => setEmail(e.target.value)}
			        required
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        type="password" 
			        placeholder="Password" 
			        value={password}
			        onChange={e => setPassword(e.target.value)}
			        required
			        />
			      </Form.Group>
			      { isActive ? <Button variant="primary" type="submit" disabled>
			        Submit
			      </Button> : <Button variant="primary" type="submit">
			        Submit
			      </Button>}
			    </Form>
	)
}