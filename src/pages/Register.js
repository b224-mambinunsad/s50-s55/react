import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Register() {

	const { user } = useContext(UserContext)
	
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);


	function registerUser(e){
		e.preventDefault()

		checkEmail(e)
		// Clearing the input fields and states

	}

	function checkEmail(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json"
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Email Already Exist",
					icon: "error",
					text: "Email is already exist please try again!"
				})
			}else{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if(data){
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Successully Register",
							icon: "success",
							text: "You have successfully registered"
						})
					}else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again later"
						})
					}
				})
			}
		})
	}

	useEffect(() => {
		if((email !== "" && password1 !== "" && password2!== "" && firstName !== "" && lastName !== "" && mobileNo.length > 10) && (password1 === password2)) {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName,email, mobileNo, password1, password2]);



	return (
		(user.id !== null) ?
			<Navigate to='/courses' />
			:
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group className="mb-3" controlId="userFirstName">
				  <Form.Label>First Name</Form.Label>
				  <Form.Control 
				  	type="text" 
				  	placeholder="Enter first name" 
				  	value={firstName} 
				  	onChange={e => setFirstName(e.target.value)}
				  	required 
				  />
				</Form.Group>

				<Form.Group className="mb-3" controlId="userLastName">
				  <Form.Label>Last Name</Form.Label>
				  <Form.Control 
				  	type="text" 
				  	placeholder="Enter first name" 
				  	value={lastName} 
				  	onChange={e => setLastName(e.target.value)}
				  	required 
				  />
				</Form.Group>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email" 
			        	value={email} 
			        	onChange={e => setEmail(e.target.value)}
			        	required 
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userMobileNo">
			        <Form.Label>Mobile No</Form.Label>
			        <Form.Control 
			        	type="text" 
			        	placeholder="Mobile Number" 
			        	value={mobileNo} 
			        	onChange={e => setMobileNo(e.target.value)}
			        	required 
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value={password1}
			        	onChange={e => setPassword1(e.target.value)}
			        	required
			        />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password" 
			        	value={password2}
			        	onChange={e => setPassword2(e.target.value)}
			        	required
			        />
			      </Form.Group>
			      
			      {isActive ? <Button variant="primary" type="submit" id="submitBtn">
			        Submit
			      </Button> : <Button variant="primary" type="submit" disabled>
			        Submit
			      </Button> }
			</Form>
	)
}