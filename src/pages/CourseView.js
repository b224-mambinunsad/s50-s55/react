import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function CourseView(){

	// Allows us to gaing access to methods that will allow us to redirect a use to a different page after enrolling to a course
	const navigate = useNavigate();

	// The "useParams" hook allow us to retrieve the courseId passed via the URL params.
	const { courseId } = useParams();
	const { user } = useContext(UserContext)


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState();

	const enroll =  async (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId:courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successully enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course"

				})

				navigate('/courses')

			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"

				})
			}
		})
		.catch((error) => {
			console.log(error)
		})

		// try{
		// 	const res =  await fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
		// 	method: "POST",
		// 	headers: {
		// 		'Content-Type': "application/json",
		// 		Authorization: `Bearer ${localStorage.getItem('token')}`
		// 	},
		// 	body: JSON.stringify({
		// 		courseId:courseId
		// 	})
		// 	})
		// 	const data = res.json()

		// 	console.log(data)

		// 	if(data === true) {
		// 		Swal.fire({
		// 			title: "Successully enrolled",
		// 			icon: "success",
		// 			text: "You have successfully enrolled for this course"

		// 		})
		// 	}else {
		// 		Swal.fire({
		// 			title: "Something went wrong",
		// 			icon: "error",
		// 			text: "Please try again"

		// 		})
		// 	}

		// }catch(error){
		// 	Swal.fire({
		// 		title: "Error",
		// 		icon: "error",
		// 		text: error

		// 	})

		// }
	}

	useEffect(() => {
		console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	},[courseId])

	return (
		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}}>
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>{price}</Card.Text>
					        <Card.Subtitle>Class Schedule:</Card.Subtitle>
					        <Card.Text>8:00 AM - 5:00 PM</Card.Text>
					        {
					        	(user.id !== null) ?
					        		<Button className="bg-primary" onClick={() => enroll(courseId)} >Enroll</Button>
					        	:
					        		<Button className="btn btn-danger" as={Link} to='/login'>Login to Enroll</Button>
					        }
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}