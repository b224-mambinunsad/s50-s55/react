import { Row, Col } from 'react-bootstrap';
import Banner from '../components/Banner'


export default function  Error() {
	return (
		<Row>
			<Col className="text-center">
				<Banner />
			</Col>
		</Row>
	)
}