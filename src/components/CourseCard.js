import { useState, useEffect } from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

	// Checks to see if the data was successfully passed.
	// console.log(props)
	// Every component recieves information in a form of object
	// console.log(typeof props)
	// Using the dot notation access the property to retrivee value/data
	// console.log(props.courseProp.name)
	// Checks if we can retrieve data from courseProps
	// console.log(courseProp)

	/*
		Use the state hook for this component to be able to store its state.
		State are used to keep track of the information related to individual components.

		Syntax:
			consts [getter, setter] = useState(initialGetterValue)
	*/

	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	// function enroll() {
	// 	if(count === 30 && seat === 0){
	// 		alert("No more seats")
	// 	}else{
	// 		setCount(count + 1)
	// 		setSeat(seat - 1)
	// 	}
	// }

	function enroll() {
		if(count < 30) {
			setCount(count + 1)
			console.log('Enrollee: ' + count)
			setSeats(seats - 1)
			console.log('Seats: ' + seats)
		} // else {
			// alert("No more seats available.")
		// }
	}

	useEffect(() => {
		if(seats === 0){
			alert("No more seats available")
		}
	}, [seats])

	const { name, description, price, _id } = courseProp

	return (


		<Row>
			<Col xs={12} md={6}>
				<Card className="cardHighlight2 p-3">
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>{price}</Card.Text>
				        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}