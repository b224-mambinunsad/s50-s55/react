import { Row, Col, Button, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom'

export default function Banner(props){

	return (

		<Row>
			<Col className="p-5">
				{props.location === "Home" ?
				<h1>Zuitt Coding Bottcamp</h1> :
				<Image src="https://res.cloudinary.com/dzevfwrwa/image/upload/v1672969876/404-img_mlpuwd.png" />}
				<p>{props.location === "Home" ? "Opportunities for everyone, everywhere." : "The page you are looking cannot be found"}</p>
				{props.location === "Home" ?
				<Button variant="primary">Enroll Now!</Button> :
				<Button as={Link} to="/" variant="primary">Back to Home</Button>}
			</Col>
		</Row>
	)
}